﻿<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xhtml="http://www.w3.org/1999/xhtml">

	<xsl:output media-type="html" indent="yes" omit-xml-declaration="yes" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" />
	
	<xsl:template match="/">
		<xsl:apply-templates select="favorieten" />
	</xsl:template>
	
	<xsl:template match="favorieten">
		<html>
			<head>
				<title><xsl:value-of select="@titel" /></title>
				<link rel="stylesheet" href="style.css" type="text/css" />
				<link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png" />
				<link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png" />
				<link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png" />
				<link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png" />
				<link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png" />
				<link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png" />
				<link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png" />
				<link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png" />
				<link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png" />
				<link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png" />
				<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png" />
				<link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png" />
				<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png" />
				<link rel="manifest" href="/manifest.json" />
				<meta name="msapplication-TileColor" content="#ffffff" />
				<meta name="msapplication-TileImage" content="/ms-icon-144x144.png" />
				<meta name="theme-color" content="#ffffff" />
			</head>
			<body>
				<h1><xsl:value-of select="@titel" /></h1>
				<div>
				<xsl:apply-templates select="kolom" />
				</div>
			</body>
		</html>
	</xsl:template>
	
	<xsl:template match="kolom">
		<xsl:element name="div">
			<xsl:attribute name="class">
				<xsl:text>div</xsl:text>
				<xsl:value-of select="position()" />
			</xsl:attribute>
			<xsl:for-each select="groep">
				<xsl:apply-templates select="." />
				<xsl:if test="position() != last()">
					<xsl:element name="br" />
				</xsl:if>
			</xsl:for-each>
		</xsl:element>
	</xsl:template>
	
	<xsl:template match="groep">
		<table cellpadding="3" cellspacing="1">
				<thead>
					<tr>
						<td>
							<xsl:value-of select="@titel" />
						</td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<xsl:for-each select="link | ondertitel">
								<xsl:apply-templates select="." />
								<xsl:if test="position() != last()">
									<xsl:element name="br" />
								</xsl:if>
							</xsl:for-each>
						</td>
					</tr>
				</tbody>
			</table>
	</xsl:template>
	
	<xsl:template match="link">
		<xsl:element name="a">
			<xsl:attribute name="href">
				<xsl:value-of select="." />
			</xsl:attribute>
			<xsl:attribute name="target">
				<xsl:text>_blank</xsl:text>
			</xsl:attribute>
			<xsl:value-of select="@naam" />
		</xsl:element>
	</xsl:template>
	
	<xsl:template match="ondertitel">
		<span>
			<xsl:value-of select="." />
		</span>
	</xsl:template>

</xsl:stylesheet>